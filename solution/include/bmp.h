#include <stdio.h>
#include <stdlib.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#define BMP_SIGNATURE 0x4d42
#define BMP_BITS 24
#define BMP_PLANES 1
#define BMP_INFO_HEADER_SIZE 40

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t type;
    uint32_t file_size;
    uint32_t reserved;
    uint32_t offset;

    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t size_image;
    uint32_t x_resolution;
    uint32_t y_resolution;
    uint32_t n_colours;
    uint32_t important_colours;
};
#pragma pack(pop)


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_MEMORY_ERROR,
    READ_INVALID_HEADER,
    READ_FILE_READING_ERROR,
    READ_HEADER_READ_ERROR,
    READ_IMAGE_READ_ERROR,
};

enum read_status read_bmp_file_image(FILE* input_file, struct image* image);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_FILE_WRITING_ERROR,
    WRITE_INCORRECT_IMAGE,
    WRITE_HEADER_WRITING_ERROR,
};

enum write_status write_bmp_file_image(FILE* output_file, const struct image image);

#endif //IMAGE_TRANSFORMER_BMP_H
