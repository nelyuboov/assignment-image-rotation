#include <stdbool.h>

#include "image.h"

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

bool rotate_image(const struct image source_image, struct image* rotated_image); // return true if success

#endif //IMAGE_TRANSFORMER_ROTATION_H
