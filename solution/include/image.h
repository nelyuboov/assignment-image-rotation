#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

bool create_image(const uint32_t width, const uint32_t height, struct image* image);
void free_image(struct image image);


#endif //IMAGE_TRANSFORMER_IMAGE_H
