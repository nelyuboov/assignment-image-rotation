#include "rotation.h"

static struct pixel* get_pixel(const uint32_t w, const uint32_t h, const struct image image){
    if(image.data == NULL) return NULL;
    return image.data + h * image.width + w;
}

bool rotate_image(const struct image source_image, struct image* rotated_image) { // return true if success
    if(source_image.data == NULL) return false;
    const bool create_image_success = create_image(source_image.height, source_image.width, rotated_image);
    if(!create_image_success) return false;

    for(uint32_t h = 0; h < rotated_image->height; ++h){
        for(uint32_t w = 0; w < rotated_image->width; ++w){
            *get_pixel(w,  h, *rotated_image) = *get_pixel(h, rotated_image->width - w - 1, source_image);
        }
    }
    return true;
}
