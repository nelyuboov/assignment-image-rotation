#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "rotation.h"

#define ARG_COUNT 3

enum exit_status {
    OK,
    WRONG_ARGUMENTS,
    FILE_READING_ERROR,
    IMAGE_READING_ERROR,
    IMAGE_WRITING_ERROR,
    IMAGE_PROCESSING_ERROR,
};

int main( int argc, char** argv ) {

    if(argc < ARG_COUNT) { // check program arguments count
        printf("Wrong arguments. Expected <source image path> <output image path>");
        return WRONG_ARGUMENTS;
    }

    // open source file
    FILE* in = fopen(argv[1], "rb");
    if(in == NULL) {
        printf("Cannot read input file. Path - %s", argv[1]);
        return FILE_READING_ERROR;
    }

    // open destination file
    FILE* out = fopen(argv[2], "wb");
    if(out == NULL){
        printf("Cannot read output file. Path - %s", argv[2]);
        return FILE_READING_ERROR;
    }

    struct image source_image = {0};
    const enum read_status read_status = read_bmp_file_image(in, &source_image);
    if(read_status) {
        printf("Error while reading image\n");
        fclose(in);
        fclose(out);
        free_image(source_image);
        return IMAGE_READING_ERROR;
    }


    // rotate image
    struct image rotated_image = {0};
    const bool rotate_status = rotate_image(source_image, &rotated_image);
    if(!rotate_status){
        printf("Error while transforming image\n");
        fclose(in);
        fclose(out);
        free_image(source_image);
        free_image(rotated_image);
        return IMAGE_PROCESSING_ERROR;
    }

    // save image to file
    const enum write_status write_status = write_bmp_file_image(out, rotated_image);
    if(write_status) {
        printf("Error while writing image\n");
        fclose(in);
        fclose(out);
        free_image(source_image);
        free_image(rotated_image);
        return IMAGE_WRITING_ERROR;
    }

    fclose(in);
    fclose(out);
    free_image(source_image);
    free_image(rotated_image);

    return OK;
}
