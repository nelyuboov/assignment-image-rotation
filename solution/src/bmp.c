#include "bmp.h"


static enum read_status read_bmp_header(FILE* input_file, struct bmp_header* header){
    if(fread(header, sizeof (struct bmp_header), 1, input_file) != 1)
        return READ_HEADER_READ_ERROR;
    return READ_OK;
}

static enum write_status write_bmp_header(FILE* output_file, const struct bmp_header header){
    if(fwrite(&header, sizeof(struct bmp_header), 1, output_file) != 1) return WRITE_HEADER_WRITING_ERROR;
    return WRITE_OK;
}

static enum read_status verify_bmp_header(const struct bmp_header header){
    if(header.bit_count != BMP_BITS) return READ_INVALID_BITS;
    if(header.type != BMP_SIGNATURE) return READ_INVALID_SIGNATURE;
    if(header.height < 1 || header.width < 1) return READ_INVALID_HEADER;
    return READ_OK;
}

static uint8_t calc_padding_bytes(const uint32_t image_width){
    const uint32_t width_bytes = image_width * sizeof (struct pixel);
    if(width_bytes % 4 == 0) return 0;
    return 4 - width_bytes % 4;
}

enum read_status read_bmp_file_image(FILE* input_file, struct image* image){
    // check if file is correct
    if(input_file == NULL) {
        printf("Input file path incorrect. Cannot read image\n");
        return READ_FILE_READING_ERROR;
    }

    // read bmp header from file
    struct bmp_header header = {0};
    const enum read_status header_read_status = read_bmp_header(input_file, &header);
    if(header_read_status){
        printf("Cannot read header\n");
        return header_read_status;
    }

    // verify header
    const enum read_status header_verify_status = verify_bmp_header(header);
    if(header_verify_status) {
        printf("Incorrect header\n");
        return header_verify_status;
    }

    // create image
    const bool image_create_status = create_image(header.width, header.height, image);
    if(!image_create_status){
        printf("Error while creating image. Cannot allocate memory\n");
        return READ_MEMORY_ERROR;
    }

    // read image
    const uint32_t padding_bytes = calc_padding_bytes(image->width);
    for(uint32_t h = 0; h < header.height; ++h){
        // read pixels row
        if(fread(image->data + h * header.width /* row bias */, sizeof (struct pixel), header.width, input_file) != header.width) return READ_IMAGE_READ_ERROR;
        // skip padding
        uint32_t t = 0;
        if(fread(&t, 1 /* 1 byte */, padding_bytes, input_file) != padding_bytes) return READ_IMAGE_READ_ERROR;
    }

    return READ_OK;
}

static struct bmp_header create_bmp_header_from_image(const struct image image){
    struct bmp_header header = {0};
    header.type = BMP_SIGNATURE;
    header.bit_count = BMP_BITS;

    header.header_size = BMP_INFO_HEADER_SIZE;
    header.size_image = (sizeof (struct pixel) * image.width + calc_padding_bytes(image.width)) * image.height;
    header.file_size = header.header_size + header.size_image;

    header.offset = sizeof (struct bmp_header);

    header.width = image.width;
    header.height = image.height;

    header.planes = BMP_PLANES;

    return header;
}

enum write_status write_bmp_file_image(FILE* output_file, const struct image image){
    // check if file is correct
    if(output_file == NULL) {
        printf("Output file path incorrect. Cannot read image\n");
        return WRITE_FILE_WRITING_ERROR;
    }

    // check if image is correct
    if(image.data == NULL) {
        printf("Incorrect image\n");
        return WRITE_INCORRECT_IMAGE;
    }

    // create bmp header
    const struct bmp_header header = create_bmp_header_from_image(image);

    // write header to file
    const enum write_status header_write_status = write_bmp_header(output_file, header);
    if(header_write_status){
        printf("Error while writing header to file\n");
        return header_write_status;
    }

    // write pixels to file
    const uint32_t trash_value = 0;
    const uint32_t padding_bytes = calc_padding_bytes(image.width);
    for(uint32_t h = 0; h < image.height; ++h){
        // write pixels row
        if(fwrite(image.data + h * image.width, sizeof (struct pixel), image.width, output_file) != image.width) return WRITE_FILE_WRITING_ERROR;
        // write padding
        if(fwrite(&trash_value /* as trash value*/, 1, padding_bytes, output_file) != padding_bytes) return WRITE_FILE_WRITING_ERROR;
    }

    return WRITE_OK;
}
