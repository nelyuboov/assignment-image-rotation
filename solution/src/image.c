#include "image.h"

bool create_image(const uint32_t width, const uint32_t height, struct image* image){
    image->width = width;
    image->height = height;

    image->data = malloc(sizeof (struct pixel) * width * height);
    if(image->data == NULL) return false;

    return true;
}

void free_image(struct image image){
    free(image.data);
}
